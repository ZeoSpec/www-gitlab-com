---
layout: markdown_page
title: "Category Direction - Workspaces"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Not applicable](#maturity) |
| Labels | [Workspaces](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWorkspaces) |

## Workspaces

It's not enough to use groups to manage enterprise activity on GitLab.com; instead, we should consider how we might create a sub-instance level namespace within a GitLab instance. We can call this a GitLab Space or a GitLab Workspace.

The benefit to a customer would be removing the need to operate within the confines of a group. Instead, an enterprise could operate in a dedicated workspace, creating users (including admin users) scoped specifically to that workspace, isolated from the rest of the instance. This mechanism could also remove the need for group-level things and remove the barrier between self-managed and GitLab.com.

This is particularly valuable for GitLab.com but also valuable for self-managed instances seeking a barrier of separation between departments (for example, a large tech company might want a separate workspace for highly sensitive projects).

## Target audience and experience

Whilst we can assume that all users would benefit from the Workspaces experience, the target personas we are focusing on are Team Leaders, [Group Owners](https://docs.gitlab.com/ee/user/permissions.html#permissions) & System Administrators.

### Current focus

The current focus is to understand what an initial MVC might look like for a GitLab Space/Workspace that looks to try and solve the following problem: "As a Group Owner, I don’t have full autonomy over administrative decisions and tasks, as the group level permissions are not comprehensive enough." ([example](https://gitlab.com/gitlab-org/gitlab/issues/12411))."

Please see the [GitLab Spaces](https://gitlab.com/groups/gitlab-org/-/epics/1606) epic.

<!-- ## What's next & why -->

## Maturity

For the moment, Workspaces are considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions. 

## How you can help

As with any category in GitLab, it's dependent on your ongoing feedback and contributions. Here's how you can help:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/603).
1. Find issues in this category accepting merge requests. [Here's an example query](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=user%profile&label_name[]=Accepting%20merge%20requests)!

<!-- ## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD

-->
