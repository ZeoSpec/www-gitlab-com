---
layout: markdown_page
title: "Category Direction - Code Analytics"
---

- TOC
{:toc}

## Manage Stage

| Stage| Maturity |
| --- | --- |
| [Manage](/direction/manage/) | [Minimal](/direction/maturity/) |

### Introduction and how you can help
Thank you for visiting the direction page for Code Analytics. This strategy is a work in progress.

### Overview

While we strive to provide recommendations and checks around [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) in real time and through static analysis, we believe there are a lot more insights about our code that can be drawn from our repositories. Moreover, our codebase is usually a complex system, which has to be understood well in order to be managed well under time and money constraints. With more and more companies adopting agile practices and suffering from high attrition, understanding and modifying code can become a challenging task.

### What's Next & Why

Our strategy for this area is currently being re-evaluated. One direction that we're exploring is the broad idea of [project health](https://gitlab.com/gitlab-org/gitlab/issues/196181), to help engineering managers consider areas of risk.

### Target Audience and Experience

We believe the target audience for Code Analytics is Engineering Management, who seeks to understand their codebase and to take data driven prioritization and management decisions.

### Maturity Plan
This category is currently at the `Minimal` maturity level and our next maturity target is `Viable`. Please see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend) and related [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=group%3A%3Aanalytics&label_name[]=devops%3A%3Amanage&label_name[]=Category%3ACode%20Analytics).

